# spring-log-template

## What is spring-log-template
`spring-log-template` 

* Provides a default template for the `logback-spring.xml`.
* All you have to do is to include the dependency
* Note that for this to work, you cannot have `logback-spring.xml` in your project's resource folder; else that will take precedence
* The template provides 3 appenders 
    * CONSOLE
    * ROLLING_FILE and
    * GELF (Graylog)
* The default root logger level is `INFO`.
* You can customized the log levels in your project's `application.yml`. 

E.g.
```
logging:
  file: log/${spring.application.name}.log
  level:
    org.springframework.boot: INFO
    com.battletech.maddog: DEBUG
  pattern:
    level: "xid=%X{xid} %5p"  
```
    
* The following are a few system variables that can be configured .
    * `LOG_FILE_ENABLED` - default is true. ROLLING_FILE logger will not be there if set to `false`.
    * `LOG_FILE_AS_JSON` - default is false. Will log as json for ROLLING_FILE logger if set to `true`.
    * `GRAYLOG_HOST` - default is localhost
    * `GRAYLOG_PORT` - default is 12201
    * `GRAYLOG_PROTO` - default is `UDP`. We support either `TCP` or `UDP`. So if value is not `UDP`, we will assume it is `TCP`.
    * `GRAYLOG_ENABLED` - default is false.
    * `GRAYLOG_TAGS_SYS` - default to `unknown`. 

## Declaring the dependency
```Groovy
    repositories {
        ...
        maven { url 'https://raw.githubusercontent.com/alvin9906/maven-repo/master' }  // maven-repo at github
        ...
    }    
     
   dependencies {
        .....
        compile 'com.battletech.maddog:spring-log-template:0.2.2-spring-boot-2.1.6' 
        .....
   }
```

## Additional 
In this repo, there is a `docker-compose.yml` file that will enable you to run the graylog server in a docker container.

Note:
* You will need to create a docker network for the first time
```
docker create network graylog-nw
```

* You will need to run in compatibility mode as we are trying to limit resources when using docker compose 3.
```
  --compatibility             If set, Compose will attempt to convert deploy
                              keys in v3 files to their non-Swarm equivalent
```                              

```
docker-compose --compatibility up -d or
docker-compose --compatibility down 
```

* Graylog web ui is configured in docker compose to at `http://127.0.0.1:9000`.
* The default configured credentials is `admin/admin`.


  
