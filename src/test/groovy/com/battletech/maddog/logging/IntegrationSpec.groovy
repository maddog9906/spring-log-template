package com.battletech.maddog.logging

import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import spock.lang.Specification

@SpringBootTest(classes = LoggingAppTest, webEnvironment = SpringBootTest.WebEnvironment.NONE)
@ActiveProfiles("test")
class IntegrationSpec extends Specification {
}
