package com.battletech.maddog.logging

import groovy.util.logging.Slf4j
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.groovy.template.GroovyTemplateAutoConfiguration
import org.springframework.context.annotation.ComponentScan
import org.springframework.scheduling.annotation.EnableScheduling

import javax.annotation.PostConstruct
import java.time.Instant

@SpringBootApplication
@EnableScheduling
@ComponentScan(["com.battletech.maddog.logging"])
@Slf4j
@EnableAutoConfiguration(exclude = [GroovyTemplateAutoConfiguration])
class LoggingAppTest {

    static void main(String[] args) {
        SpringApplication.run(LoggingAppTest, args)
    }

    @PostConstruct
    void init() {
        log.info("Setting system timezone to UTC...")
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"))
        log.info("Current system timezone is now ${TimeZone.default}")
        log.info("Current system time is ${Instant.now()}")
    }
}
