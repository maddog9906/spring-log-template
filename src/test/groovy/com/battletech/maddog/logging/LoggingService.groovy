package com.battletech.maddog.logging

import groovy.util.logging.Slf4j
import org.slf4j.MDC
import org.springframework.stereotype.Service

@Service
@Slf4j
class LoggingService {

    def method1() {
        try {
            MDC.put("xid", UUID.randomUUID().toString())
            log.debug("Oh my god !!!!")
            log.warn("On no!!!!")
            log.error("Bloody hell!!!")
        } finally {
            MDC.remove("xid")
        }
    }
}
