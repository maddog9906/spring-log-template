package com.battletech.maddog.logging

import org.springframework.beans.factory.annotation.Autowired
import spock.lang.Subject

class LoggingSpec extends IntegrationSpec {

    @Subject
    @Autowired
    LoggingService loggingService

    def "test logging"() {
        when:
        (0..100).each {
            loggingService.method1()
        }

        then:
        assert true

    }
}
